#/usr/bin/python

import os
import subprocess
import sys
import os.path
#import pyodbc
import pytds

# Database credentials
driver = 'SQL Server'
server = sys.argv[1]
database = sys.argv[2]
username = sys.argv[3]
password = sys.argv[4]

#affectedFilesList = sys.argv[5:] 
affectedFilesList = sys.argv[5].split('\n')

print("-------------------")

for a in affectedFilesList:
    print(a)

print("-------------------")

# If there is more that one changed file, grab all the files but not the script itself.
#affectedFilesList = list(affectedFiles.split("\n"))

# For loop, do this job for all files in the list
for affectedFile in affectedFilesList:

    print("-----------------------------------------------------------------------------------------")

    # Print file name to the console
    #print("Current file: " + affectedFile)
    print("Current file: ")
    print(affectedFile)
    print("----------------")

    # Get file extension
    ext = os.path.splitext(affectedFile)[1]

    print("File extension is: " + ext)
    print(ext)

    # Check if file extension is 'sql'
    if 'sql' in ext:

        # get view name
        viewName = affectedFile.split(".", 1)[0]
        print("View name: " + viewName)

        # connection to database with all the information
        with pytds.connect(server, database, username, password) as conn:
            with conn.cursor() as cursor:

                print("Connection to DB succeeded...")

                # allow python code to execute SQL command in database session.
                cursor = conn.cursor()

                # Drop view if exist
                dropQuery = "DROP VIEW IF EXISTS " + viewName
                cursor.execute(dropQuery)
                print("Deleting view (if exists)...")

                if os.path.exists(viewName+".sql"):

                    print("File found at specified path...")

                    # build view full name
                    sqlView = open(viewName+".sql", "r")

                    # remove all white spaces
                    query = " ".join(sqlView.readlines())

                    # show the query on the screen
                    print(query)

                    cursor.execute(query)
                    print("Creating view: " + viewName)


            # save changes permanent
            conn.commit()
            #cursor.commit()
            print("Saving changes...")
